function validate(employees, schema) {

    let finalResult = {
        ok: true,
        message: 'success'
    };

    for (const item of employees) {
        const result = validateChild(item, schema);

        if (result) {
            finalResult = result;
            break;
        }
    }

    return finalResult;
}

function validateChild(employee, schema) {

    const requiredProperties = schema.employee.filter(item => item.required === true);
    const requiredPropertiesNames = requiredProperties.map(item => item.name);

    for (const propertyName of requiredPropertiesNames) {
        if (!employee.hasOwnProperty(propertyName)) {
            return { ok: false, message: `${propertyName} is required` };
        }
    }

    // get the keys of employee.
    const keys = Object.keys(employee);

    // i was looping through the schema object first, rather than the employee data object.
    // so if the salary is before the name in the schema, but in the employee data, it was after,
    // salary would be checked first even if it was after the name in the employee.

    for (const key of keys) {

        const schemaItem = schema.employee.find(item => item.name == key);
        const schemaType = schemaItem.type;
        // schemaType = 'string'

        if (
            (schemaType != 'array.employee' && typeof employee[key] != schemaType)
            || (schemaType == 'array.employee' && !Array.isArray(employee[key]))
        ) {
            return { ok: false, message: `type ${schemaType} expected for ${key}` };
        }
    }

    const propertyNames = schema.employee.map(item => item.name);

    for (const key of keys) {

        // check if schema has this property of the employee.
        if (!propertyNames.includes(key)) {

            // if schema doesn't have the property but the employee have, then its an error. it shouldn't be allowed.
            // error message #3.
            return { ok: false, message: `unexpected property ${key}` };
        }
    }

    for (const key of keys) {

        // check if any property is array or not.
        if (Array.isArray(employee[key])) {

            // if array, then loop through every item of that array.
            for (const item of employee[key]) {

                // call validateChild() recursively to validate an item of the array.
                // this function only return error, if no error, then this returns undefined.
                const result = validateChild(item, schema);

                // if any error?
                if (result) {

                    // return the error. remember, return statement just prevents any further execution of the
                    // function it is called in, so all for loops will just stop.
                    // if this return is in the recursive function, then the parent function's execution will
                    // still continue, unless return is called on the parent as well.
                    return result;
                }
            }
        }
    }
}

const employees = [{
    "name": "alice",
    "title": 'hello',
    "salary": 100,
    "reports": [{
        "name": "alice",
        "title": "controller",
        "salary": 40,
        "reports": [{
            "name": "alice",
            "title": "controller",
            "salary": '40',
            "reports": true
        }],
    }],
}];

const schema = {
    employee: [
        {name: 'name', required: true, type: 'string'},
        {name: 'title', required: true, type: 'string'},
        {name: 'reports', required: false, type: 'array.employee'},
        {name: 'salary', required: true, type: 'number'},
        {name: 'remote', required: false, type: 'boolean'},
        {name: 'subordinates', required: false, type: 'array.employee'}
    ]
};

console.log(validate(employees, schema));

// BOOM
