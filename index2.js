function validate(employees, schema) {
    // success message.
    let finalResult = {
        ok: true,
        message: 'success'
    };

    for (const item of employees) {
        const result = validateChild(item, schema);
        if (result) {
            finalResult = result;
            break;
        }
    }

    return finalResult;
}

function validateChild(employee, schema) {
    const requiredProperties = schema.employee.filter(item => item.required === true);
    const requiredPropertiesNames = requiredProperties.map(item => item.name);

    for (const propertyName of requiredPropertiesNames) {
        if (!employee.hasOwnProperty(propertyName)) {
            // error message.
            return { ok: false, message: `${propertyName} is required` };
        }
    }

    const keys = Object.keys(employee);

    for (const key of keys) {

        const schemaItem = schema.employee.find(item => item.name == key);
        const schemaType = schemaItem.type;
        // schemaType = 'string'

        if (
            (schemaType != 'array.employee' && typeof employee[key] != schemaType)
            || (schemaType == 'array.employee' && !Array.isArray(employee[key]))
        ) {
            return { ok: false, message: `type ${schemaType} expected for ${key}` };
        }
    }

    const propertyNames = schema.employee.map(item => item.name);

    for (const key of keys) {
        if (!propertyNames.includes(key)) {
            // error message.
            return { ok: false, message: `unexpected property ${key}` };
        }
    }

    for (const key of keys) {
        if (Array.isArray(employee[key])) {
            for (const item of employee[key]) {
                const result = validateChild(item, schema);
                if (result) {
                    return result;
                }
            }
        }
    }
}

const employees = [{
    "name": "alice",
    "title": 'hello',
    "salary": 100,
    "reports": [{
        "name": "alice",
        "title": "controller",
        "salary": 40,
        "reports": [{
            "name": "alice",
            "title": "controller",
            "salary": '40',
            "reports": true
        }],
    }],
}];

const schema = {
    employee: [
        {name: 'name', required: true, type: 'string'},
        {name: 'title', required: true, type: 'string'},
        {name: 'reports', required: false, type: 'array.employee'},
        {name: 'salary', required: true, type: 'number'},
        {name: 'remote', required: false, type: 'boolean'},
        {name: 'subordinates', required: false, type: 'array.employee'}
    ]
};

console.log(validate(employees, schema));
